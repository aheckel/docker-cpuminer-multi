FROM debian:buster-slim AS builder

ARG CPUMINER_VERSION
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update \
 && apt install -y automake autoconf curl git pkg-config libcurl4-openssl-dev libjansson-dev libssl-dev libgmp-dev zlib1g-dev make g++ \
 && git clone --depth 1 --branch "v${CPUMINER_VERSION}-multi" https://github.com/tpruvot/cpuminer-multi /tmp/cpuminer-multi

RUN cd /tmp/cpuminer-multi \
 && if [ "aarch64" = "$(uname -m)" ]; then \
      cd /tmp/cpuminer-multi; \
      ./autogen.sh; \
      ./nomacro.pl; \
      ./configure --disable-assembly CFLAGS="-Ofast -march=native" --with-crypto --with-curl; \
      make;\
    else \
      ./build.sh; \
    fi

FROM debian:buster-slim

LABEL maintainer="Alexander Heckel <alexander.heckel@icloud.com>"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update \
 && apt install -y libcurl4-openssl-dev libjansson-dev libssl-dev libgmp-dev zlib1g-dev \
 && rm -rf /var/lib/lists/*

COPY --from=builder /tmp/cpuminer-multi/cpuminer /usr/local/bin

ENTRYPOINT ["/usr/local/bin/cpuminer"]
CMD ["--help"]

