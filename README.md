# Multi Arch Build

```bash
export CPUMINER_VERSION=1.3.5
docker buildx build --build-arg CPUMINER_VERSION --platform linux/arm64,linux/amd64 -t aheckel/cpuminer-multi:"${CPUMINER_VERSION}"  --push .
```
